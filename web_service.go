package main

import (
	"encoding/json"
	"fmt"
	"github.com/labstack/echo/v4"
	"net/http"
	"strconv"
)

// Declare pointer of Employee
var employee Employee = Employee{"Tuan", 1, "male"}

//data of employee
var dataJson []byte

// pointer to Employee
var pEmployee = &Employee{}

// Employee define struct Employee
type Employee struct {
	Name   string `json:"name"`
	Id     int    `json:"id"`
	Gender string `json:"gender"`
}

// declare struct Employee
var buffEmployee Employee = employee

// parse payload
func parsePayload(buffEmployee Employee) Employee {
	json.Unmarshal(dataJson, buffEmployee)
	fmt.Println(buffEmployee)
	return buffEmployee
}

// get information of a employee && parse payload
func getInfo(c echo.Context) error {
	buffEmployee = parsePayload(buffEmployee)
	return c.JSON(http.StatusOK, buffEmployee)
}

// create new connection of a employee
func createUser(c echo.Context) error {
	dataJson, _ = json.Marshal(employee)
	if err := c.Bind(dataJson); err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}
	return c.JSON(http.StatusCreated, dataJson)
}

// get Id of an employee
func getIdEmployee(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	var dataServer Employee = employee
	fmt.Println(id)
	return c.JSON(http.StatusOK, dataServer)
}

// Update information of an employee
func updateInfo(c echo.Context) error {
	buffEmployee.Name = "Huy"
	dataJson, _ := json.Marshal(buffEmployee)
	fmt.Println(string(dataJson))
	return c.JSON(http.StatusOK, dataJson)
}

//delete information of  employee
func deleteUser(c echo.Context) error {
	employee = *pEmployee
	return c.NoContent(http.StatusNoContent)
}

func main() {
	e := echo.New()
	e.POST("/employee", createUser)
	e.GET("/employee", getInfo)
	e.PUT("/employee/:id", updateInfo)
	e.GET("/employee/:id", getIdEmployee)
	e.DELETE("/employee/:id", deleteUser)

	// server runs on port 1323
	e.Logger.Fatal(e.Start(":1323"))

}
